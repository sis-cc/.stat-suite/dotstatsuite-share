import { omit } from 'ramda'
import njwt from 'njwt'
import Model from './model'
import { status, isPending } from '../charts'
import { ID, parseISO8601, ChartNotFoundError, getChartTitle } from './utils'
import { HTTPError } from '../utils/errors'

const NAME = 'charts'

const makeIn = ({ email, data, confirmUrl, logo, content, subject, css }) => ({
  _id: ID(),
  email,
  confirmUrl,
  data: JSON.stringify(data),
  status: status.PENDING,
  createdAt: new Date(),
  lastAccessedAt: new Date(),
  accessCount: 0,
  logo,
  content,
  subject,
  css,
})

export class ChartModel extends Model {
  constructor(name, collection, config) {
    super(name, collection)
    this.config = config
  }

  makeOut({ noData } = {}) {
    return input => {
      const chart = {
        id: input._id,
        ...omit(['_id', 'data'], input),
      }

      chart.expire = new Date(+chart.lastAccessedAt + parseISO8601(this.config.outOfDate))
      const data = JSON.parse(input.data)
      if (isPending(chart) || noData) {
        chart.data = {
          title: getChartTitle(data),
          dataflowId: data.config?.dataflowId,
          type: data.type,
        }
      } else {
        chart.data = data
      }
      return chart
    }
  }

  async parseToken(token) {
    return new Promise((resolve, reject) => {
      njwt.verify(token, this.config.secretKey, (err, njwtoken) => {
        if (err) return reject(new HTTPError(401, err.message))
        return resolve(njwtoken)
      })
    })
  }

  async getFromToken(token) {
    const njwtoken = await this.parseToken(token)
    return await this.get(njwtoken.body.sub)
  }

  async listFromToken(token) {
    const njwtoken = await this.parseToken(token)
    return await this.list(njwtoken.body.email)
  }

  async confirm(id) {
    const lastAccessedAt = new Date()
    const expire = new Date(+lastAccessedAt + parseISO8601(this.config.outOfDate))
    await this.collection.updateOne({ _id: ID(id) }, { $set: { status: status.CONFIRMED, expire, lastAccessedAt } })
  }

  async add(input) {
    const chart = makeIn(input)
    await this.collection.insertOne(chart)
    return this.makeOut()(chart)
  }

  async get(id) {
    const chart = await this.collection.findOne({ _id: ID(id) }, { projection: { status: 1 } })
    if (!chart) throw new ChartNotFoundError(id)
    const lastAccessedAt = new Date()
    const expire = new Date(+lastAccessedAt + parseISO8601(this.config.outOfDate))
    if (isPending(chart)) await this.collection.updateOne({ _id: ID(id) }, { $set: { expire, lastAccessedAt } })
    else await this.collection.updateOne({ _id: ID(id) }, { $inc: { accessCount: 1 }, $set: { lastAccessedAt } })
    return this.makeOut()(await this.collection.findOne({ _id: ID(id) }))
  }

  async list(email) {
    const charts = await this.collection.find({ email, status: { $ne: status.PENDING } }).toArray()
    return charts.map(this.makeOut({ noData: true }))
  }

  async loadAll() {
    const charts = await this.collection.find({}).toArray()
    return charts.map(this.makeOut({ noData: true }))
  }

  async purge() {
    const { deletedCount } = await this.collection.deleteMany({ expire: { $lt: new Date() } })
    return { deletedCount }
  }

  async delete({ token, id }) {
    const njwtoken = await this.parseToken(token)
    const email = njwtoken.body.email
    const chart = await this.get(id)
    if (!chart || chart.email !== email) throw new ChartNotFoundError(id)
    await this.collection.deleteOne({ _id: ID(id) })
    return { id }
  }

  async deleteAll({ token }) {
    const njwtoken = await this.parseToken(token)
    const email = njwtoken.body.email
    const { deletedCount } = await this.collection.deleteMany({ email })
    return { deletedCount }
  }
}

export default async ({ mongo: { database }, config }) => {
  const collection = database.collection(NAME)
  return [
    NAME,
    new ChartModel(NAME, collection, config),
    {
      indexes: [
        [{ email: 1 }, { partialFilterExpression: { status: status.PENDING }, expireAfterSeconds: config.chartsTTL }],
      ],
    },
  ]
}

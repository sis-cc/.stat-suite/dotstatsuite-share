import express from 'express';
import helmet from 'helmet';
import http from 'http';
import bodyParser from 'body-parser';
import compression from 'compression';
import cors from 'cors';
import debug from '../../debug';
import apiConnector from '../../services/api/connector';
import adminConnector from '../../services/admin/connector';
import healthConnector from '../../services/healthcheck/connector';
import errors from './middlewares/errors';

const expressPino = require('express-pino-logger')({ logger: debug });
const getUrl = server => `http://${server.address().address}:${server.address().port}`;

export default ctx => {
  const {
    config,
    services: { admin, api, healthcheck },
  } = ctx();
  const {
    server: { host, port },
    maxChartSize,
  } = config;
  const app = express();
  const httpServer = http.createServer(app);

  const promise = new Promise(resolve => {
    app
      .use(compression())
      .use(cors())
      .use(bodyParser.json({ limit: maxChartSize }))
      .use(helmet.hidePoweredBy())
      .use(helmet.hsts())
      .use('/healthcheck', healthConnector('healthcheck', healthcheck))
      .use(expressPino)
      .use('/api', apiConnector(api))
      .use('/admin', adminConnector(admin))
      .use(errors);

    httpServer.listen(port, host, () => {
      httpServer.url = getUrl(httpServer);
      debug.info(`server started on ${httpServer.url}`);
      resolve(ctx({ httpServer }));
    });
  });

  return promise;
};

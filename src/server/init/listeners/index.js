import { reduce } from 'ramda';
import initCharts from './charts';

const ressources = [initCharts];
const init = ctx => reduce((acc, initFn) => acc.then(initFn), Promise.resolve(ctx), ressources);
export default init;

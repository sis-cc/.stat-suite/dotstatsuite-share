import * as R from 'ramda';

export default ({ link, headerMsg, subHeaderMsg, copyMsg, confirmationText, buttonMsg, bodyColor, buttonColor }) => {
  return `
<table width="600" cellpadding="0" cellspacing="0" border="0" style="color:#222; font-family: Arial, Helvetica, sans-serif; font-size:16px;">
  <tbody>
    <tr>
      <td width="10" style="background: ${bodyColor};">
        &nbsp;
      </td>
      <td style="background: ${bodyColor}; font-size:14px; line-height:16px; color: #222; text-align: center;" valign="top";>
        <p style="color: #222; font-size: 32px; font-family: Arial, Helvetica, sans-serif; line-height: 34px;">${headerMsg}</p>
        <p style="color: #222; font-size: 16px; font-family: Arial, Helvetica, sans-serif; line-height: 21px;">
          ${subHeaderMsg}
          <br><br><br>
          <a rel="noopener noreferrer" style="background-color:${buttonColor};color:white;text-decoration:none;padding: 15px 10px 15px 10px;border-radius:3px;font-size: 14px; font-family: Arial, Helvetica, sans-serif;" href="${link}">${buttonMsg}</a>
          <br><br><br>
          ${copyMsg}<br>
          <div style="color:black;font-size: 10px; font-family: Arial, Helvetica, sans-serif; line-height: 1; max-width: 500px; margin: auto;">
            <a href="${link}">${R.reduce((acc, word) => R.concat(acc, `${word}<br>`), '', R.splitEvery(70, link))}</a>
          </div>
        </p>
        <p>
          <svg class="MuiSvgIcon-root jss148" focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M12 22c1.1 0 2-.9 2-2h-4c0 1.1.89 2 2 2zm6-6v-5c0-3.07-1.64-5.64-4.5-6.32V4c0-.83-.67-1.5-1.5-1.5s-1.5.67-1.5 1.5v.68C7.63 5.36 6 7.92 6 11v5l-2 2v1h16v-1l-2-2z"></path></svg>
          ${confirmationText}
          </span>
        <p/>
      </td>
    </tr>
  </tbody>
</table>
`;
};

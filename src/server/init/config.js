import '../env'
import { getSMTPEnv } from '../utils'

export default ctx => {
  const server = {
    host: process.env.HOST || '0.0.0.0',
    port: Number(process.env.PORT) || 3007,
  }
  const siteUrl = process.env.SITE_URL || `http://${server.host}:${server.port}`

  const config = {
    appId: 'share',
    isProduction: process.env.NODE_ENV === 'production',
    outOfDate: process.env.OUT_OF_DATE || 'P1Y',
    mailFrom: process.env.MAIL_FROM,
    hfrom: process.env.HFROM,
    gitHash: process.env.GIT_HASH || 'local',
    env: process.env.NODE_ENV,
    apiKey: process.env.API_KEY || 'secret',
    secretKey: process.env.SECRET_KEY || 'secret',
    chartsTTL: 3600 * 4,
    maxChartSize: '5000kb',
    siteUrl,
    server,
    smtp: getSMTPEnv(process.env),
    mongo: {
      url: process.env.MONGODB_URL || 'mongodb://localhost:27017',
      dbName: process.env.MONGODB_DATABASE || 'share',
    },
  }

  return ctx({ startTime: new Date(), config })
}

import '@babel/polyfill'
import cfonts from 'cfonts'
import { initResources } from 'jeto'
import debug from './debug'
import initHttp from './init/http'
import initServices from './services'
import initEmails from './init/emails'
import initListeners from './init/listeners'
import initConfig from './init/config'
import initReporter from './init/reporter'
import initMongo from './init/mongo'
import initModels from './init/models'

const cfontsSettings = {
  font: 'block',
  align: 'left',
  colors: ['cyanBright', 'white'],
  background: 'transparent',
  letterSpacing: 1,
  lineHeight: 1,
  space: true,
  maxLength: '0',
  gradient: false,
  independentGradient: true,
  transitionGradient: true,
  env: 'node',
}

const ressources = [
  initConfig,
  ctx => {
    debug.info(ctx().config, 'running config')
    return ctx
  },
  initReporter,
  initMongo,
  initModels,
  initServices,
  initHttp,
  initEmails,
  initListeners,
]

initResources(ressources)
  .then(() => debug.info('🤖 server started'))
  .then(() => cfonts.say('share is up', cfontsSettings))
  .catch(err => {
    debug.error(err, 'Boot failed')
    process.exit()
  })

import evtX from 'evtx';
import debug from '../../debug.js';
import initHealthcheck from './healthcheck.js';
import { initServices } from '../utils.js';

const services = [initHealthcheck];

export default ctx => {
  const healthcheck = evtX(ctx).configure(initServices(services));
  debug.info('healthcheck service up.');
  return ctx({ services: { ...ctx().services, healthcheck } });
};

export const formatInput = maker => ctx => {
  const { input } = ctx
  return Promise.resolve({ ...ctx, input: maker(input) })
}

export const validate = schema => ctx => {
  const { input } = ctx
  return schema
    .validate(input, {
      strict: false,
      // stripUnknown: true,
    })
    .then(request => ({ ...ctx, input: request }))
}

export const emitEvent = name => ctx => {
  ctx.emit(name, ctx)
  return Promise.resolve(ctx)
}

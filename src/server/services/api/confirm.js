import { URL } from 'url'
import { emitEvent } from './utils'
import { HTTPError } from '../../utils/errors'
import { isPending } from '../../charts'

const SERVICE_NAME = 'confirm'

export const confirm = {
  async get({ id, token }) {
    const { models } = this.globals
    id ||= token
    try {
      if (!id) throw new HTTPError(400, `Token search params is required to confirm a chart!`)
      const chart = await models.charts.getFromToken(id)
      if (!isPending(chart)) throw new HTTPError(400, `Chart '${chart.id}' already confirmed`)
      await models.charts.confirm(chart.id)
      const url = new URL(chart.confirmUrl)
      url.searchParams.append('chartId', chart.id)
      return { url }
    } catch (e) {
      if (e instanceof HTTPError) throw e
      throw new HTTPError(404, e.message)
    }
  },
}

const init = evtx => {
  evtx.use(SERVICE_NAME, confirm)
  evtx.service(SERVICE_NAME).after({
    get: [emitEvent('chart:confirmed')],
    post: [emitEvent('chart:confirmed')],
  })
}

export default init

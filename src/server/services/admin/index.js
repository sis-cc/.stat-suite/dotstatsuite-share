import R from 'ramda'
import evtX from 'evtx'
import initCharts from './charts'
import { checkApiKey } from './utils'

const allServices = [initCharts]
const initServices = evtx => R.reduce((acc, service) => acc.configure(service), evtx, allServices)
const init = ctx => {
  const admin = evtX(ctx())
    .configure(initServices)
    .before(checkApiKey)

  return ctx({ services: { ...ctx().services, admin } })
}

export default init

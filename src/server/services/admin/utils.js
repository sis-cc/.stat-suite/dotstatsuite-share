import HTTPStatus from 'http-status';
import { HTTPError } from '../../utils/errors';

export const checkApiKey = ctx => {
  const {
    locals: {
      req: {
        headers: { 'x-api-key': xApiKey },
        query: { 'api-key': qApiKey },
      },
    },
    globals: {
      config: { apiKey },
    },
  } = ctx;
  if (apiKey !== (xApiKey || qApiKey)) return Promise.reject(new HTTPError(HTTPStatus.UNAUTHORIZED));
  return Promise.resolve(ctx);
};

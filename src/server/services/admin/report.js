export const service = {
  name: 'report',

  get() {
    const {
      globals: { report },
    } = this;
    return Promise.resolve(report());
  },
};

const init = evtx => {
  evtx.use(service.name, service).service(service.name);
};

export default init;

const SERVICE_NAME = 'charts'

export const start = {
  async get() {
    const { models } = this.globals
    return await models.charts.loadAll()
  },

  async delete() {
    const { models } = this.globals
    return await models.charts.purge()
  },
}

const init = evtx => {
  evtx.use(SERVICE_NAME, start)
}

export default init
